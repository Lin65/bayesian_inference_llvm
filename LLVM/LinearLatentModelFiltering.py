from sklearn.decomposition import PCA, FactorAnalysis
import numpy as np
import scipy

import copy

# ------------------------------------------------------------------------------------------
# BayesianLinearModel - helps in performing Bayesian linear inversions
# ------------------------------------------------------------------------------------------
class BayesianLinearRegression(object):

    # ------------------------------
    # based initialization
    # ------------------------------
    def __init__(self, A, b, Pee):
        """

        Consider the following linear model

        d = Ax + b + e

        where

        - e is a zero-mean normally distributed r.v. with covariance matrix Pee.
        - x is assumed to be zero-mean normally distributed r.v. with unity covariance matrix
          (this can be changed by calling the function

        :param A: linear transport operator
        :param b: offset
        :param Pee: covariance matrix of the noise
        """

        # TODO: perform sanity checks to make sure that all the dimensions match
        # reshape the data point
        b = b.reshape((A.shape[0],1))

        self._A = A
        self._b = b
        self._Pee = Pee
        self._Pee_inv = np.linalg.inv( Pee )

        self._nd = A.shape[0]
        self._nx = A.shape[1]

        # generate prior and calculate temporal variables
        self.reset_prior()

        # TODO: the class needs to be updated to accommodate the identification of Pee

    # ------------------------------
    # based initialization
    # ------------------------------
    def set_latent_var(self, mu_x, Pxx):
        """

        :param mu_x: mean
        :param Pxx: covariance
        """

        # TODO: perform sanity checks to make sure that all the dimensions match
        # reshape the data point
        mu_x = mu_x.reshape((self._nx,1))

        self._mu_x = mu_x
        self._Pxx = Pxx

        # update temporal variables
        self._update_tmp_var()

        # I need data to update the log-evidence
        self._logev = None

    # ------------------------------
    # reset the prior to zero mean and identity covariance
    # ------------------------------
    def reset_prior(self):

        self._mu_x = np.zeros((self._nx,1))
        self._Pxx = np.eye(self._nx)

        # update temporal variables
        self._update_tmp_var()

        # I need data to update the log-evidence
        self._logev = None

    # ------------------------------
    # cache the temporal quantities to compute posterior statistics and log-evidence
    # ------------------------------
    def _update_tmp_var(self):

        self._M_d = self._A.dot(self._Pxx).dot(self._A.T) + self._Pee
        self._M_d_inv = np.linalg.inv(self._M_d)

        # self._P_tmp = np.linalg.pinv( self._A.T.dot(self._Pee_inv).dot(self._A) )
        # self._P_sum = self._P_tmp + self._Pxx
        # self._P_sum_inv = np.linalg.inv( self._P_sum )

    # ------------------------------
    # update the model using a piece of data
    # ------------------------------
    def fit(self, D):

        # TODO: sanity check on data dimensions
        # reshape the data point
        D = D.reshape((self._nd,1))

        mu_x_prior = self._mu_x
        Pxx_prior = self._Pxx

        # update mean and covariance with posterior statistics
        self._mu_x = mu_x_prior + Pxx_prior.dot(self._A.T).dot(self._M_d_inv).dot(D-self._A.dot(mu_x_prior)-self._b)
        self._Pxx = Pxx_prior - Pxx_prior.dot(self._A.T).dot(self._M_d_inv).dot(self._A).dot(Pxx_prior)
        
        # get log-evidence
        # mu_tmp = self._P_tmp.dot(self._A.T).dot(self._Pee_inv).dot(D-self._b)
        #
        #err_tmp = mu_tmp - mu_x_prior


        # not sure where this comes from
        # self._logev = -0.5*np.log(2.0*np.pi)*np.float(self._nx) \
        #     -0.5*np.log( np.linalg.det(self._P_sum) ) \
        #     -0.5*err_tmp.T.dot(self._P_sum_inv).dot(err_tmp)
        #
        # print 'logev 1 = ', self._logev

        # P_sum = self._A.dot(Pxx_prior).dot(self._A.T) + self._Pee
        # P_sum_inv = np.linalg.inv( P_sum )
        err_tmp = D-self._A.dot(mu_x_prior)-self._b

        self._logev = -0.5 * np.log(2.0 * np.pi) * np.float(self._nd) \
              - 0.5 * np.log(np.linalg.det(self._M_d)) \
              - 0.5 * err_tmp.T.dot(self._M_d_inv).dot(err_tmp)

        #print 'logev 2 = ', self._logev

        # update temporal variables
        self._update_tmp_var()

    # ------------------------------
    # get the mean and covariance of the qoi
    # ------------------------------
    def get_mean_cov_logev(self):

        return { 'mu': self._mu_x, 'cov': self._Pxx, 'logev': self._logev }


# ------------------------------------------------------------------------------------------
# BaseLinearLatentModel - this will be used to derive the rest of classes
# ------------------------------------------------------------------------------------------
class BaseLinearLatentModel(object):

    # ------------------------------
    # based initialization
    # ------------------------------
    def __init__(self, llm_type='PPCA', verbose=True):
        """

        :param lm_type: linear latent model created by PPCA, FA
        """
        self.llm_type = llm_type

        self.verbose = verbose

        # dimensionality
        self.nx = 0     # number of qoi variables
        self.nd = 0     # number of observable variables
        self.ny = 0     # dimensionality of entire vector (just nx+nd)
        self.nz = 0     # number of latent variables

        # samples
        self.N = 0          # number of samples / models
        self.y_samp = None  # store all the samples

        # linear model for y = [x d]
        self.mu_y = None
        self.W_y = None
        self.Psi_y = None
        self.Psi_y_inv = None

        # linear model for x
        self.mu_x = None
        self.W_x = None
        self.Psi_x = None
        self.Psi_x_inv = None

        # linear model for d
        self.mu_d = None
        self.W_d = None
        self.Psi_d = None
        self.Psi_d_inv = None
        

    # ------------------------------
    # given the samples create the linear model
    # ------------------------------
    def fit(self, y_samp, nx, nz, axis=0):
        """
        Given the sample matrix generate the linear models

        :param y_samp: matrix containing the samples of x and y
        :param nx: number of qoi variables
        :param axis: samples are on axis=0 (default) otherwise use axis=1
        :param nz: number of latent variables
        """

        if not(axis in [0,1]):
            raise NameError('axis can only be 0 or 1')

        if self.verbose:
            print self.__class__.__name__, ' : fit model using - ', self.llm_type

        # save variables
        self.nx = nx
        self.nz = nz
        self.ny = y_samp.shape[1-axis]
        self.nd = self.ny - self.nx
        self.N = y_samp.shape[axis]

        # transpose the matrix if needed
        if axis == 0:
            self.y_samp = y_samp
        else:
            self.y_samp = y_samp.T

# PPCA ########################################################################

        if self.llm_type == 'PPCA':
            # perform probabilistic PCA
            pca = PCA(svd_solver = 'full')
            pca.copy = True
            pca.whiten = True

            # if required perform cross validation to detect the number of factors
            if self.nz == 'cv':
                # maximum no of comp is the minimum between number of samples
                # and the number of features in the data
                max_components = min(self.N, self.ny)

                pca_scores = []
                for n in range(1, max_components):
                    pca.n_components = n

                    try:
                        pca_scores.append(np.mean(cross_val_score(pca, self.y_samp, verbose=0)))
                    except ValueError:
                        raise RuntimeError('PPCA - CV has failed')
                        #pca_scores = np.arange(1, max_components)
                        break

                # number of factors set to the best cross-validation result
                self.nz = np.argmax(pca_scores)+1
                #print 'No-comp (PCA) = ', self.nz

            # perform factor analysis
            pca.n_components = self.nz
            pca.fit(self.y_samp)

            # extract linear model for y = [x d]
            self.mu_y = pca.mean_.reshape((self.ny, 1))
            #self.W_y = pca.components_.T
            self.W_y = np.dot(pca.components_.T, np.diag(np.sqrt(pca.explained_variance_ - pca.noise_variance_)))
            #print 'pca.explained_variance_: ',pca.explained_variance_
            #print 'pca.noise_variance_: ',pca.noise_variance_
           
            self.Psi_y = np.diag(np.ones(self.ny)*(pca.noise_variance_+np.finfo(float).eps)) # covariance of noise
            self.Psi_y_inv = np.diag(np.ones(self.ny)/(pca.noise_variance_+np.finfo(float).eps))

            #print 'Noise variance = ', pca.noise_variance_

# FA ##########################################################################

        elif self.llm_type == 'FA':
            # perform factor analysis
            fa = FactorAnalysis()
            fa.copy = True

            # if required perform cross validation to detect the number of factors
            if self.nz == 'cv':
                # maximum no of comp is the minimum between number of samples
                # and the number of features in the data
                max_components = min(self.N, self.ny)

                fa_scores = []
                for n in range(1,max_components):
                    fa.n_components = n
                    #print cross_val_score(fa, X, verbose=0)
                    fa_scores.append(np.mean(cross_val_score(fa, self.y_samp, verbose=0)))

                # number of factors set to the best cross-validation result
                self.nz = np.argmax(fa_scores)+1
                #print 'No-comp (FA) = ', self.nz

            # perform factor analysis
            fa.n_components = self.nz
            fa.fit(self.y_samp)

            # extract linear model for y = [x d]
            self.mu_y = fa.mean_.reshape((self.ny, 1))
            self.W_y = fa.components_.T
            self.Psi_y = np.diag(fa.noise_variance_)
            #print fa.noise_variance_
            self.Psi_y_inv = np.diag(1.0/fa.noise_variance_)



# PCCA2 ########################################################################
#
#         elif self.llm_type == 'PCCA2':
#
#             # first get the sample covariance
#
#             pca_xd = PCA(n_components=self.nz)
#             pca_xd.copy = True
#             pca_xd.fit(self.y_samp)
#             cov_xd = pca_xd.get_covariance()
#
#             pca_x = PCA(n_components=self.nz)
#             pca_x.copy = True
#             pca_x.fit(self.y_samp[:,0:self.nx])
#             cov_x = pca_x.get_covariance()
#
#             pca_d = PCA(n_components=self.nz)
#             pca_d.copy = True
#             pca_d.fit(self.y_samp[:,self.nx:self.ny])
#             cov_d = pca_d.get_covariance()
#
#             # initialize parameters
#             sig_x = 1.0
#             sig_d = 1.0
#             Bx = np.ones(())
#
#
#             # noise covariance is not diagonal
#             #Psi_x = sqrtSx.dot(Ux).dot(np.eye(self.nz)-P).dot(Ux.T).dot(sqrtSx.T)
#             #Psi_d = sqrtSd.dot(Ud).dot(np.eye(self.nz)-P).dot(Ud.T).dot(sqrtSd.T)
#
#             # noise covariance is not diagonal
#             #Psi_x = sqrtSx.dot(np.eye(self.nx)-Ux.dot(P.dot(P)).dot(Ux.T)).dot(sqrtSx.T)
#             #Psi_d = sqrtSd.dot(np.eye(self.nd)-Ud.dot(P.dot(P)).dot(Ud.T)).dot(sqrtSd.T)
#
#             # make it diagonal - this is not right
#             Psi_x = np.diag(np.diag(sqrtSx.dot(np.eye(self.nx)-Ux.dot(P.dot(P)).dot(Ux.T)).dot(sqrtSx.T)))
#             Psi_d = np.diag(np.diag(sqrtSd.dot(np.eye(self.nd)-Ud.dot(P.dot(P)).dot(Ud.T)).dot(sqrtSd.T)))
#
#             self.mu_y = np.reshape(np.mean(self.y_samp, axis=0),(self.ny,1))
#             self.W_y = np.vstack([Wx, Wd])
#             self.Psi_y = scipy.linalg.block_diag(Psi_x, Psi_d)
#             self.Psi_y_inv = scipy.linalg.inv(self.Psi_y)

###############################################################################

        # extract linear model for x
        self.mu_x = self.mu_y[0:self.nx].reshape((self.nx, 1))
        self.W_x = self.W_y[0:self.nx, ]
        self.Psi_x = self.Psi_y[0:self.nx, 0:self.nx]
        self.Psi_x_inv = self.Psi_y_inv[0:self.nx, 0:self.nx]

        # extract linear model for d
        self.mu_d = self.mu_y[self.nx:self.ny].reshape((self.nd, 1))
        self.W_d = self.W_y[self.nx:self.ny, ]
        self.Psi_d = self.Psi_y[self.nx:self.ny, self.nx:self.ny]
        self.Psi_d_inv = self.Psi_y_inv[self.nx:self.ny, self.nx:self.ny]


# ------------------------------------------------------------------------------------------
# DirectLinearLatentModel - this will use just the linear latent models
#                           for Bayesian inversion, thus the posterior
#                           distribution is Gaussian
# ------------------------------------------------------------------------------------------
class DirectLinearLatentModel(BaseLinearLatentModel):

    # ------------------------------
    # initialization
    # ------------------------------
    def __init__(self, llm_type='PPCA'):
        """

        :param lm_type: linear latent model created by PPCA, FA
        """

        # base init
        super(DirectLinearLatentModel, self).__init__(llm_type)

        # store Gaussian posterior distribution of latent variable
        self.mu_z_post = None
        self.Sigma_z_post = None
        self.logev = None

        # store Gaussian posterior distribution of qoi
        self.mu_x_post = None
        self.Sigma_x_post = None

        # store Gaussian posterior for the data
        self.mu_y_post = None
        self.Sigma_y_post = None

    # ------------------------------
    # given the samples create the linear model
    # ------------------------------
    def fit(self, y_samp, nx, nz, axis=0):
        """
        Given the sample matrix generate the linear models

        :param y_samp: matrix containing the samples of x and y
        :param nx: number of qoi variables
        :param axis: samples are on axis=0 (default) otherwise use axis=1
        :param nz: number of latent variables
        """
        # base fit
        super(DirectLinearLatentModel, self).fit(y_samp, nx, nz, axis)

        # initialize the posterior mean and covariance of the latent variable
        self.mu_z_post = np.zeros((self.nz,1))
        self.Sigma_z_post = np.eye(self.nz)

        # propagate inference
        self.__propagate_inference__()

    # ------------------------------
    # propagate inference and obtain the Gaussian mixture for x
    # ------------------------------
    def __propagate_inference__(self):

        self.mu_x_post = self.W_x.dot(self.mu_z_post) + self.mu_x
        self.Sigma_x_post = self.W_x.dot(self.Sigma_z_post).dot(self.W_x.T) + self.Psi_x

        self.mu_y_post = self.W_y.dot(self.mu_z_post) + np.reshape(self.mu_y,(self.ny,1))
        self.Sigma_y_post = self.W_y.dot(self.Sigma_z_post).dot(self.W_y.T) + self.Psi_y

    # ------------------------------
    # update the latent variable model using the data
    # ------------------------------
    def update_lvm(self, D):

        if self.verbose:
            print self.__class__.__name__, ' : update LVM using data'

        D = D.reshape((self.nd,1))

        self._update_lvm = True

        var_z_prior = 1
        var_z_prior_old = 0

        updated = False

        ###############
        # COMPELETELY ISOTROPIC
        ##############
        while np.abs(var_z_prior-var_z_prior_old) > 10**-3:

            var_z_prior_old = var_z_prior

            # E-step
            tmp_model = BayesianLinearRegression( self.W_d, self.mu_d, self.Psi_d )
            tmp_model.set_latent_var(np.zeros(self.nz), np.eye(self.nz)*var_z_prior)
            tmp_model.fit( D )

            tmp_results = tmp_model.get_mean_cov_logev()
            mu_z = tmp_results['mu']
            Sigma_z = tmp_results['cov']
            updated = True

            # M-step
            var_z_prior = (np.trace(Sigma_z) + mu_z.T.dot(mu_z))/self.nz

            if var_z_prior < 1.0:
                var_z_prior = 1.0

        if self.verbose:
            print self.__class__.__name__, ' : update (EM) var_prior = ', var_z_prior

        if not(updated):
            raise 'Something is going on!'

        # save the mean and the covariance of the latent variable
        self._mean_z = mu_z
        self._cov_z = Sigma_z

        self.update_model(D)

    # ------------------------------
    # update the latent variable model using the data
    # ------------------------------
    def update_lvm_noise(self, D):
        if self.verbose:
            print self.__class__.__name__, ' : update LVM using data'

        D = D.reshape((self.nd, 1))

        self._update_lvm = False

        # start wit
        U = D.dot(D.T)
        Pmax = self.W_d.dot(self.W_d.T)
        diffMat = U - Pmax
        alpha = 1.0 / self.nd * np.matrix.trace(diffMat.dot(self.Psi_d_inv))

        # save the new noise matrix
        print 'alpha = ', alpha

        if alpha > 1.0:
            #self.Psi_x = alpha * self.Psi_x
            self.Psi_d = alpha * self.Psi_d
            self.Psi_y = scipy.linalg.block_diag(self.Psi_x, self.Psi_d)

            self.Psi_y_inv = scipy.linalg.inv(self.Psi_y)
            self.Psi_d_inv = self.Psi_y_inv[self.nx:self.ny, self.nx:self.ny]

        # generate models and update them
        self.update_model(D)

    # ------------------------------
    # update models based on data
    # ------------------------------
    def update_model(self, D):
        """
        Update model

        :param D: data that will be used to update the linear model
        """

        tmp_model = BayesianLinearRegression(self.W_d, self.mu_d, self.Psi_d)
        tmp_model.set_latent_var( self.mu_z_post, self.Sigma_z_post )
        tmp_model.fit( D )

        # save data
        tmp_results = tmp_model.get_mean_cov_logev()
        self.mu_z_post = tmp_results['mu']
        self.Sigma_z_post = tmp_results['cov']
        self.logev = tmp_results['logev']

        # propagate inference
        self.__propagate_inference__()

    # ------------------------------
    # get the mean of the qoi
    # ------------------------------
    def get_mean_qoi(self):

        return self.mu_x_post

    # ------------------------------
    # generate samples
    # ------------------------------
    def generate_samples(self, no_samp=0):

        if self.verbose:
            print self.__class__.__name__, ' : generate samples'

        if no_samp == 0:
            no_samp = self.N

        samps = np.zeros((no_samp,self.nx))

        for i in range(no_samp):

            samps[i,:] = np.random.multivariate_normal(self.mu_x_post.flatten(), self.Sigma_x_post)

        return samps


# ------------------------------------------------------------------------------------------
# EnsembleLinearLatentModel - this will provide an ensemble from the posterior distribution
#                             using the linear latent model
# ------------------------------------------------------------------------------------------
class EnsembleLinearLatentModel(BaseLinearLatentModel):

    # ------------------------------
    # initialization
    # ------------------------------
    def __init__(self, llm_type='PPCA', verbose=False):
        """

        :param lm_type: linear latent model created by PPCA, FA
        """
        # base init
        super(EnsembleLinearLatentModel, self).__init__(llm_type, verbose=verbose)

        # lvm
        self._mean_z = None
        self._cov_z = None

        # store all the models
        self.models = []
        
        # store all the models II
        self.models_II = []

        # store Gaussian mixture for x
        self.GMMx = []

        # store Gaussian mixture for y
        self.GMMy = []

        # figure out how this is used
        self._update_lvm = False

    # ------------------------------
    # given the samples create the linear model
    # ------------------------------
    def fit(self, y_samp, nx, nz, axis=0):
        """
        Given the sample matrix generate the linear models

        :param y_samp: matrix containing the samples of x and y
        :param nx: number of qoi variables
        :param axis: samples are on axis=0 (default) otherwise use axis=1
        :param nz: number of latent variables
        """

        if self.verbose:
            print self.__class__.__name__, ' : fit model using - ', self.llm_type

        # base fit
        super(EnsembleLinearLatentModel, self).fit(y_samp, nx, nz, axis)

        self.__generate_models__()
        self.__propagate_inference_x__()
        self.__propagate_inference_y__()

    # ------------------------------
    # update the latent variable model using the data
    # ------------------------------
    def update_lvm(self, D):

        if self.verbose:
            print self.__class__.__name__, ' : update LVM using data'

        D = D.reshape((self.nd,1))

        self._update_lvm = True

        # optimize for var_psi_d
        var_psi_d = self.Psi_d[0,0]    # all are the same for PPCA
        var_psi_d_old = 0
        var_psi_d_state = var_psi_d

        var_z_prior = 1
        var_z_prior_old = 0

        updated = False


        ###############
        # COMPELETELY ISOTROPIC
        ##############
        while np.abs(var_z_prior-var_z_prior_old) > 10**-3:

            var_z_prior_old = var_z_prior

            # E-step
            tmp_model = BayesianLinearRegression( self.W_d, self.mu_d, self.Psi_d )
            tmp_model.set_latent_var(np.zeros(self.nz), np.eye(self.nz)*var_z_prior)
            tmp_model.fit( D )

            tmp_results = tmp_model.get_mean_cov_logev()
            mu_z = tmp_results['mu']
            Sigma_z = tmp_results['cov']
            updated = True

            # M-step
            var_z_prior = (np.trace(Sigma_z) + mu_z.T.dot(mu_z))/self.nz

            if var_z_prior < 1.0:
                var_z_prior = 1.0

        if self.verbose:
            print self.__class__.__name__, ' : update (EM) var_prior = ', var_z_prior



        if not(updated):
            raise 'Something is going on!'

        # # if is smaller than what we have found with the samples then
        # # do not change it
        # if var_psi_d < var_psi_d_state:
        #     var_psi_d = var_psi_d_state
        #
        # # do no propagate the model error to the state
        # self.Psi_x = np.eye(self.nx)*var_psi_d_state
        # self.Psi_d = np.eye(self.nd)*var_psi_d
        # self.Psi_y = sci.linalg.block_diag(self.Psi_x, self.Psi_d)
        # self.Psi_x_inv = np.linalg.inv(self.Psi_x)
        # self.Psi_d_inv = np.linalg.inv(self.Psi_d)
        # self.Psi_y_inv = np.linalg.inv(self.Psi_y)

        # TODO - I do change the prior but I will only save the posterior
        # the only contribution of this is for the posterior and measurement error
        #self._cov_z = np.eye(self.nz)*var_z_prior

        # save the mean and the covariance of the latent variable
        self._mean_z = mu_z
        self._cov_z = Sigma_z

        # generate models and update them
        self.__generate_models__()
        self.update_models(D)


    # ------------------------------
    # update the latent variable model using the data
    # ------------------------------
    def update_lvm_noise(self, D):
        if self.verbose:
            print self.__class__.__name__, ' : update LVM using data'

        D = D.reshape((self.nd, 1))  

        self._update_lvm = False

        # start wit
        U = D.dot(D.T)
        Pmax = self.W_d.dot(self.W_d.T)
        diffMat = U - Pmax
        alpha = 1.0/self.nd * np.matrix.trace( diffMat.dot(self.Psi_d_inv) )
        alpha = 1
        #print 'alpha', alpha

        #print diffMat
        # save the new noise matrix
        #print 'alpha = ', alpha
        #self.orig_Psi_y = copy.deepcopy(self.Psi_y)
        if alpha > 1.0:
            #print alpha
            self.Psi_x = alpha * self.Psi_x
            self.Psi_d = alpha * self.Psi_d
            self.Psi_y = scipy.linalg.block_diag(self.Psi_x, self.Psi_d)

            self.Psi_y_inv = scipy.linalg.inv(self.Psi_y)
            self.Psi_d_inv = self.Psi_y_inv[self.nx:self.ny, self.nx:self.ny]
            #self.Psi_x_inv = self.Psi_y_inv[0:self.nx, 0:self.nx]
        
        
        #print alpha
        # generate models and update them
        self.__generate_models__()   # uncomment !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        self.update_models(D)

    # ------------------------------
    # NEW: generate alternative models based on initial samples
    # ------------------------------
    def __generate_models__(self):

        if self.verbose:
            print self.__class__.__name__, ' : generate models NEW'

        self.models = []
        self.models_II = []

        # update using the x only
        tmp_model = BayesianLinearRegression( self.W_y, self.mu_y, self.Psi_y )

        for i in range(self.N):

            if self._update_lvm:
                tmp_model.set_latent_var( self._mean_z, self._cov_z )
            else:
                tmp_model.reset_prior()

            tmp_model.fit( self.y_samp[i,:] )
            tmp_results = tmp_model.get_mean_cov_logev()

            self.models.append({'mu': tmp_results['mu'], 'cov': tmp_results['cov'], 'weight': 1.0/self.N, 'logev': tmp_results['logev']})
            
            #print 'tmp_results mu : ', tmp_results['mu'].shape
            
            self.models_II.append({'mu': self.W_y.dot(tmp_results['mu']) + self.mu_y, 'cov': self.W_y.dot(tmp_results['cov']).dot(self.W_y.T) + self.Psi_y, 'weight': 1.0/self.N})
            
    # ------------------------------
    # update models based on data
    # ------------------------------
    def update_models(self, D):
        """
        Update models

        :param D: data that will be used to update the models
        """

        if self.verbose:
            print self.__class__.__name__, ' : update models'

        # do not use the data twice
        # if self._update_lvm:
        #     raise "Data has already been used to update the latent variable model!"

        # instantiate a linear regression object
        tmp_model = BayesianLinearRegression( self.W_d, self.mu_d, self.Psi_d )
        
        #print 'w_d',self.W_d.shape

        all_logev = np.zeros((self.N,1))
        for i in range(self.N):

            tmp_model.set_latent_var( self.models[i]['mu'], self.models[i]['cov'] )
            tmp_model.fit( D )
            #print 'mu',self.models[i]['mu'].shape
            tmp_results = tmp_model.get_mean_cov_logev()

            self.models[i]['mu'] = tmp_results['mu']
            self.models[i]['cov'] = tmp_results['cov']
            #self.models[i]['weight'] = np.exp(tmp_results['logev'])
            all_logev[i] = tmp_results['logev']
            self.models[i]['logev'] = tmp_results['logev']
            
            self.models_II[i]['mu'] =  self.models_II[i]['mu'][0: self.nx] + self.models_II[i]['cov'][0: self.nx,  self.nx: (self.nx+ self.nd)].dot(np.linalg.inv(self.models_II[i]['cov'][ self.nx:( self.nx+  self.nd),  self.nx:( self.nx+  self.nd)])).dot(D.reshape(-1,1) - self.models_II[i]['mu'][self.nx:( self.nx+  self.nd)])

            self.models_II[i]['cov'] = self.models_II[i]['cov'][0:self.nx, 0:self.nx] - self.models_II[i]['cov'][0:self.nx, self.nx:(self.nx +self.nd)].dot(np.linalg.inv(self.models_II[i]['cov'][self.nx:(self.nx + self.nd), self.nx:(self.nx+self.nd)])).dot(self.models_II[i]['cov'][0:self.nx, self.nx:(self.nx+self.nd)].transpose())
            
            
            
            

        # adjust the logev such that not all of them become 0
        logev_max = np.max(all_logev)
        new_logev = all_logev - logev_max
        weights = np.exp(new_logev)/np.sum(np.exp(new_logev))

        #print 'sum weights = ', np.sum(weights)
        #print weights
        # update the weights (sum to one)
        for i in range(self.N):
            self.models[i]['weight'] = weights[i]
            
            self.models_II[i]['weight'] = weights[i]
            
            #print 'weight = ', self.models[i]['weight']

        # propagate inference
        self.__propagate_inference_x__()
        self.__propagate_inference_y__()

    # ------------------------------
    # propagate inference and obtain the Gaussian mixture for x
    # ------------------------------
    def __propagate_inference_x__(self):

        if self.verbose:
            print self.__class__.__name__, ' : propagate inference x'

        self.GMMx = []

        Sigma_z = self.models[0]['cov']   # all covariances are the same

        for i in range(self.N):

            mu_i = self.models[i]['mu']
            w_i = self.models[i]['weight']

            mu_x = self.W_x.dot(mu_i) + self.mu_x
            Sigma_x = self.W_x.dot(Sigma_z).dot(self.W_x.T) + self.Psi_x

            self.GMMx.append({'mu': mu_x, 'cov': Sigma_x, 'weight': w_i})

    # ------------------------------
    # propagate inference and obtain the Gaussian mixture for y = x & d
    # ------------------------------
    def __propagate_inference_y__(self):

        if self.verbose:
            print self.__class__.__name__, ' : propagate inference y'

        self.GMMy = []

        #print 'W_y = ', self.W_y
        #print 'mu_y = ', self.mu_y

        Sigma_z = self.models[0]['cov']   # all covariances are the same

        for i in range(self.N):

            mu_i = self.models[i]['mu']
            w_i = self.models[i]['weight']

            #print 'mu[',i,']= ', mu_i

            mu_y = self.W_y.dot(mu_i) + self.mu_y
            Sigma_y = self.W_y.dot(Sigma_z).dot(self.W_y.T) + self.Psi_y

            #print 'mu_y[',i,']= ', mu_y

            self.GMMy.append({'mu': mu_y, 'cov': Sigma_y, 'weight': w_i})

    # ------------------------------
    # get the mean of all the models
    # ------------------------------
    def get_mean_qoi(self):

        if self.verbose:
            print self.__class__.__name__, ' : get mean QoI'

        mu = np.zeros((self.nx,1))

        for i in range(self.N):

            mu_i = self.GMMx[i]['mu']
            w_i = self.GMMx[i]['weight']

            mu += w_i * mu_i

        return mu

    # ------------------------------
    # generate samples
    # ------------------------------
    def generate_samples(self, no_samp=0):

        if self.verbose:
            print self.__class__.__name__, ' : generate samples'

        if no_samp == 0:
            no_samp = self.N

        samps = np.zeros((no_samp,self.nx))

        w_post = np.zeros(self.N)

        # sample from posterior model probabilities
        for i in range(self.N):
            w_post[i] = self.GMMx[i]['weight']

        ind_Gauss = np.random.choice(range(self.N), no_samp, p=w_post)
     
        for i in range(no_samp):

            # sample from latent variable
            mu = self.models[ind_Gauss[i]]['mu']
            Sigma = self.models[ind_Gauss[i]]['cov'] +1e-10  # all variances should be the same
            #print Sigma

            z_samp = np.random.multivariate_normal(mu.reshape((self.nz,)), Sigma)

            # get samples of observable
            #eps_samp = np.random.multivariate_normal(np.zeros(self.nx), self.Psi_x)
            #y_samp = self.W_x.dot(z_samp) + self.mu_x.reshape((self.nx,)) + eps_samp
            eps_samp = np.random.multivariate_normal(np.zeros(self.nx), self.Psi_x + self.W_x.dot(Sigma).dot(self.W_x.T))
            y_samp = self.W_x.dot(mu.reshape((self.nz,))) + self.mu_x.reshape((self.nx,)) + eps_samp  
            '''
            mu_II = self.models_II[ind_Gauss[i]]['mu']
            Sigma_II = self.models_II[ind_Gauss[i]]['cov'] + 1e-10
            
            y_samp_II = np.random.multivariate_normal(mu_II.T[0],Sigma_II )
            '''
            samps[i,:] = y_samp

        return samps
