## Bayesian_Inference_LLVM

This is a python implementation for Bayesian inference approach developed in 

Xiao Lin, Gabriel Terejanu. Fast Approximate Data Assimilation for High-Dimensional Problems. 
arXiv:1708.02340

## Dependencies

This software is dependent on the following packages: numpy, scipy, sklearn


## Contents

This package contains the core classes for LLVM. It also contains various simulations discussed in Xiao Lin, Gabriel Terejanu. Fast Approximate Data Assimilation for High-Dimensional Problems. 
arXiv:1708.02340

## License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

