__author__ = 'terejanu'

import numpy as np


# ------------------------------------------------------------------------------------------
# global settings
# ------------------------------------------------------------------------------------------
dim_lorenz = 40


# ------------------------------------------------------------------------------------------
# function to integrate
# ------------------------------------------------------------------------------------------
def lorenz96(x, t):
    dx = np.zeros(dim_lorenz)

    for k in range(dim_lorenz):

        # incorporate cyclic boundary conditions
        if (k+1) >= dim_lorenz:
            x_kp1 = x[0]
        else:
            x_kp1 = x[k+1]

        if (k-1) < 0:
            x_km1 = x[dim_lorenz-1]
            x_km2 = x[dim_lorenz-2]
        else:
            x_km1 = x[k-1]
            if (k-2) < 0:
                x_km2 = x[dim_lorenz-1]
            else:
                x_km2 = x[k-2]

        dx[k] = (x_kp1 - x_km2)*x_km1 - x[k] + 8

    # print 'x = ', x
    # print 'dx = ', dx
    return dx

# ------------------------------------------------------------------------------------------
# function to integrate
# ------------------------------------------------------------------------------------------

def lorenz63(x, t):
    dim_lorenz = 3
    
    dx = np.zeros(dim_lorenz)
    sigma = 10
    b = 8./3
    r = 28
    
    dx[0] = -sigma*x[0] + sigma*x[1]
    dx[1] = -x[0]*x[2] + r*x[0] - x[1]
    dx[2] = x[0]*x[1] - b*x[2]

    return dx
    
def OneD_bimodal(x, t):
    
    dx = np.sin(x)

    return dx
    
# ------------------------------------------------------------------------------------------
# function to integrate
# ------------------------------------------------------------------------------------------
def lorenz96_model_error(x, t, const):
    dx = np.zeros(dim_lorenz)
    
    for k in range(dim_lorenz):
        
        # incorporate cyclic boundary conditions
        if (k+1) >= dim_lorenz:
            x_kp1 = x[0]
        else:
            x_kp1 = x[k+1]
        
        if (k-1) < 0:
            x_km1 = x[dim_lorenz-1]
            x_km2 = x[dim_lorenz-2]
        else:
            x_km1 = x[k-1]
            if (k-2) < 0:
                x_km2 = x[dim_lorenz-1]
            else:
                x_km2 = x[k-2]
            

        dx[k] = (x_kp1 - x_km2)*x_km1 - x[k] + const

    # print 'x = ', x
    # print 'dx = ', dx
    return dx


# ------------------------------------------------------------------------------------------
# Sample covariance matrix from Wishart distribution
# ------------------------------------------------------------------------------------------
def sample_wishart(n, std):
    # use Barlette decomposition
    L = np.eye(n) * std

    A = np.array(np.zeros((n, n)))
    for i in range(1, n + 1):
        for j in range(1, i + 1):
            if i == j:
                A[i - 1, j - 1] = np.random.chisquare(df=n - i + 1)
            else:
                A[i - 1, j - 1] = np.random.normal(loc=0.0, scale=1.0)

    W = L.dot(A).dot(A.T).dot(L.T)

    return W


# ------------------------------------------------------------------------------------------
# Gaussian marginalization
# ------------------------------------------------------------------------------------------
def margin_gauss(mu, Sigma, lastMeas):
    n = len(mu)
    m = len(lastMeas)
    d = n - m

    lastMeas = lastMeas.reshape((m,1))
    mu_a = mu[0:d].reshape((d,1))
    mu_b = mu[d:n + 1].reshape((m,1))

    Sigma_aa = Sigma[0:d, 0:d]
    Sigma_ab = Sigma[0:d, d:n + 1]
    Sigma_bb = Sigma[d:n + 1, d:n + 1]

    Sigma_bb_inv = np.linalg.inv(Sigma_bb)

    Sigma_post = Sigma_aa - Sigma_ab.dot(Sigma_bb_inv).dot(Sigma_ab.T)
    mu_post = mu_a + Sigma_ab.dot(Sigma_bb_inv).dot(lastMeas - mu_b)

    return {'mu_post': mu_post, 'Sigma_post': Sigma_post}
