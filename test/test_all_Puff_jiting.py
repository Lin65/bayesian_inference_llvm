__author__ = 'terejanu'


import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from scipy.integrate import odeint
import llmf.LinearLatentModelFiltering as llmf
import sys

plt.interactive(False)
np.random.seed(seed=50)

# ------------------------------------------------------------------------------------------
# puff model
# ------------------------------------------------------------------------------------------
def puff_model_1D(aW, aX0):
    m = 1000
    p = 0.8
    q = 0.8
    t = 1
    w = aW
    x0 = aX0
    x = 0.  
    coeff =  m/np.sqrt(2*np.pi*(p**2)*((t*w)**(2*q)))
    return coeff*np.exp(-0.5*(x0 + t*w -x)**2/((p**2)*((t*w)**(2*q))))
# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 30
no_comp = 2


nom_W = 10.0
std_W = 1.0

nom_X0 = 0.
std_X0 = 1000.


meas_var = 1

no_samples_hist = 10000
# ------------------------------------------------------------------------------------------
# generate measurements
# ------------------------------------------------------------------------------------------
no_observables = 1

true_W = 7.0
true_X0 = 1.0


true_var = np.array([true_W, true_X0])

X_meas =  puff_model_1D(true_W, true_X0) + np.random.normal(0, np.sqrt(meas_var))


# ------------------------------
# initial condition - draw N samples
# ------------------------------
X_all = np.zeros((no_samples, 2))
X_all[:, 0] = np.random.normal(loc=nom_W, scale=std_W, size=no_samples)
X_all[:, 1] = np.random.normal(loc=nom_X0, scale=std_X0, size=no_samples)
X_all_old = X_all

# ------------------------------------------------------------------------------------------
# get RMSE for a specific method
# ------------------------------------------------------------------------------------------


X_prior = X_all.copy()
X_prior_obs = np.zeros((no_samples,no_observables))

for i in range(no_samples):
    X_prior_obs[i,:] = puff_model_1D(X_prior[i,0], X_prior[i,1]) + np.random.normal(0, np.sqrt(meas_var))
X_prior_joint = np.hstack((X_prior, X_prior_obs))

# scale the samples
trans_mean = np.mean(X_prior_joint, axis=0)
trans_std = np.std(X_prior_joint, axis=0)

tile_trans_mean = np.tile(trans_mean, (no_samples, 1))
tile_trans_std = np.tile(trans_std, (no_samples, 1))
X_prior_joint_trans = (X_prior_joint - tile_trans_mean) / tile_trans_std

# scale the measurement
X_meas_trans = (X_meas - trans_mean[2:2 + no_observables]) / trans_std[2:2 + no_observables]

if True:
    llmf_en_ppca = llmf.EnsembleLinearLatentModel('PPCA')
    llmf_en_ppca.fit(X_prior_joint_trans, nx=2, nz=no_comp)
    if True:
        llmf_en_ppca.update_lvm_noise(X_meas_trans)
    else:
        llmf_en_ppca.update_models(X_meas_trans)
    X_post_trans = llmf_en_ppca.generate_samples(no_samples_hist)
    est_mu = llmf_en_ppca.get_mean_qoi()
else:
    direct_lvm = llmf.DirectLinearLatentModel('PPCA')
    direct_lvm.fit(X_prior_joint, nx=2, nz=no_comp)
    if True:
        direct_lvm.update_lvm_noise(X_meas_trans)
    else:
        direct_lvm.update_model(X_meas_trans)
    X_post_trans = direct_lvm.generate_samples(no_samples_hist)

X_post = X_post_trans * np.tile(trans_std[0:2], (no_samples_hist, 1)) + np.tile(trans_mean[0:2], (no_samples_hist, 1))

plt.figure()
plt.hist(X_post[:, 1], bins=100)
plt.show()

