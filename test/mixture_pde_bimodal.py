import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
import llmf.LinearLatentModelFiltering as llmf
import sys

plt.interactive(False)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 3000
no_comp = 2
dim  = 2
dim_x =1
# ----------------------------
# generate samples
# ----------------------------
np.random.seed(seed=70)

mu = np.random.normal(0,1,dim)
A = np.random.normal(0,1,(dim,dim))
cov = 0.5*(A + A.T) + dim*np.eye(dim)*0.5
smps_1 = np.random.multivariate_normal(mu, cov, no_samples/2)

mu = mu + [5,-10]#
#A = np.random.normal(0,1,(dim,dim))
#cov = 0.5*(A + A.T) + dim*np.eye(dim)
smps_2 = np.random.multivariate_normal(mu, cov, no_samples/2)


smps = np.concatenate((smps_1,smps_2), axis=0)


# ------------------------------
# pde
# ------------------------------
run_model = 'PPCA'

trans_mean = np.mean(smps, axis=0)
trans_std = np.std(smps, axis=0)
trans_w = np.diag(trans_std)

tile_trans_mean = np.tile(trans_mean, (no_samples, 1))
tile_trans_std = np.tile(trans_std, (no_samples, 1))
smps_trans = (smps - tile_trans_mean) / tile_trans_std


Ensemble_lvm = llmf.EnsembleLinearLatentModel(run_model)
Ensemble_lvm.fit(smps_trans, nx=dim_x, nz=no_comp)

'''
mu_post = trans_std*Ensemble_lvm.mu_y_post.T + trans_mean
Sigma_post = np.dot(np.dot(trans_w,Ensemble_lvm.Sigma_y_post), trans_w.T)

print 'mu_post: \n', mu_post 
print 'Sigma_post: \n',Sigma_post
print 'W_y: \n', direct_lvm.W_y
print 'Psi_y: \n', direct_lvm.Psi_y
print 'mu_y: \n', direct_lvm.mu_y
'''
#plt.plot(smps[:,0], smps[:,1],'b.',zorder = 8)

smps_model1 = np.random.multivariate_normal(((trans_w.dot(Ensemble_lvm.models_II[0]['mu']))+ trans_mean.reshape(-1,1)).T[0], trans_w.dot(Ensemble_lvm.models_II[0]['cov']).dot(trans_w.T), no_samples)
#plt.plot(smps_model1[:,0], smps_model1[:,1],'r.',zorder=9)

smps_model2 = np.random.multivariate_normal(((trans_w.dot(Ensemble_lvm.mu_y))+ trans_mean.reshape(-1,1)).T[0], trans_w.dot(Ensemble_lvm.W_y.dot(Ensemble_lvm.W_y.T) + Ensemble_lvm.Psi_y).dot(trans_w.T), no_samples)
#plt.plot(smps_model2[:,0], smps_model2[:,1],'g.')

'''
smps_model2 = np.random.multivariate_normal(Ensemble_lvm.W_y.dot(Ensemble_lvm.models[10]['mu']).T[0] + mu, Ensemble_lvm.W_y.dot(Ensemble_lvm.models[1]['cov']).dot(Ensemble_lvm.W_y.T) + Ensemble_lvm.Psi_y, no_samples)
plt.plot(smps_model2[:,0], smps_model2[:,1],'.')

smps_model3 = np.random.multivariate_normal(Ensemble_lvm.W_y.dot(Ensemble_lvm.models[20]['mu']).T[0] + mu, Ensemble_lvm.W_y.dot(Ensemble_lvm.models[2]['cov']).dot(Ensemble_lvm.W_y.T) + Ensemble_lvm.Psi_y, no_samples)
plt.plot(smps_model3[:,0], smps_model3[:,1],'.')
'''

mu_smps = np.zeros((dim, no_samples))
mu_smps_II = np.zeros((dim, no_samples))

for i in range(no_samples):
    mu_smps[:,i:(i+1)] = trans_w.dot(Ensemble_lvm.W_y.dot(Ensemble_lvm.models[i]['mu'])) + trans_mean.reshape(-1,1)
    #mu_smps[i] = Ensemble_lvm.models[i]['mu'][0]
#plt.scatter(mu_smps[0,:], mu_smps[1,:],color = 'k',zorder=10)

for i in range(no_samples):
    mu_smps_II[:,i:(i+1)] = trans_w.dot(Ensemble_lvm.models_II[i]['mu']) + trans_mean.reshape(-1,1)
    #mu_smps[i] = Ensemble_lvm.models[i]['mu'][0]
#plt.scatter(mu_smps_II[0,:], mu_smps_II[1,:],color = 'g',zorder=9)


fig, axes = plt.subplots(nrows = 3, ncols=1, figsize = (4,10))
axes[0].plot(smps_model2[:,0], smps_model2[:,1],'g.')
axes[0].plot(smps[:,0], smps[:,1],'b.',zorder = 8)
axes[1].scatter(mu_smps[0,:], mu_smps[1,:],color = 'k',zorder=10)
axes[1].plot(smps_model2[:,0], smps_model2[:,1],'g.')
axes[1].plot(smps[:,0], smps[:,1],'b.',zorder = 8)
axes[2].scatter(mu_smps[0,:], mu_smps[1,:],color = 'k',zorder=10)
axes[2].plot(smps_model2[:,0], smps_model2[:,1],'g.')
axes[2].plot(smps[:,0], smps[:,1],'b.',zorder = 8)
axes[2].plot(smps_model1[:,0], smps_model1[:,1],'r.',zorder=11)

plt.savefig('figures/Mixture_PDE_4.pdf',format='pdf', dpi=200)
plt.show()


#direct_lvm.update_model(X_meas_trans)
#X_post_trans = direct_lvm.generate_samples()


