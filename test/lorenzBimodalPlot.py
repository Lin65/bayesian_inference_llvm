# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 12:30:58 2017

@author: xiaolin
"""


import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

import seaborn as sns


plt.interactive(False)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 2000
no_comp = 2
meas_freq = 1

no_observables = 1

meas_var = 1.0

# ------------------------------
# time span
# ------------------------------
t_init = 0
t_final = 1.01
t_delta = 0.01
t_all = np.arange(t_init, t_final, t_delta)
ind_meas = range(meas_freq, t_all.shape[0], meas_freq)

np.random.seed(seed=40)

# ------------------------------
# true initial condition
# ------------------------------
mixture_weight = [0.5, 0.5]
model_choice = np.random.choice(np.array([0,1]), p = mixture_weight)
mixture = [[-0.2,-0.2,8],[0.2,0.2,8]]
x_init_true_I = np.random.multivariate_normal(mixture[0], np.sqrt(0.35)*np.eye(3))

x_init_true_II = np.random.multivariate_normal(mixture[1], np.sqrt(0.35)*np.eye(3))

#np.random.multivariate_normal([0.2,0.2,8], np.sqrt(0.35)*np.eye(3))
# ------------------------------
# generate measurements
# ------------------------------
x_all_true_I = odeint(lorenz63, x_init_true_I, t_all)
x_all_true_II = odeint(lorenz63, x_init_true_II, t_all)

sns.set_style('darkgrid')
    
fig, axes = plt.subplots(nrows = 3 , ncols= 1 ,figsize=(6, 6) )
for nrow in range(3):
    axes[nrow].plot(t_all, x_all_true_I[:,nrow],'b') 
    axes[nrow].plot(t_all, x_all_true_II[:,nrow],'r') 
    
    #sns.distplot(X_all[:,nrow,ncol*10 + 9],kde_kws={"shade": True}, hist = False, vertical=True, ax = axes[nrow, ncol])
    #axes[nrow, ncol].set_ylim([-50,50])
    #axes[nrow, ncol].set_xticklabels([])
    '''        
            if ncol ==0:
                axes[nrow, ncol].locator_params(axis='y',nbins=4)
                axes[nrow, ncol].set_ylabel('$x_{%d}$'%(nrow + 1))
            if ncol !=0:   
                axes[nrow, ncol].set_yticklabels([])
                
            if nrow == 2:
                t= t_all[ncol*10 + 9]
                axes[nrow, ncol].set_xlabel('$t = {%.1f}s$'%t)
     '''           
            #plt.tick_params(
            #    axis='x',          # changes apply to the x-axis
            #    which='both',      # both major and minor ticks are affected
           #     bottom='off',      # ticks along the bottom edge are off
            #    top='off')         # ticks along the top edge are off
                #labelbottom='off') # labels along the bottom edge are off
plt.savefig('figures/lorenzbimodal.pdf',format='pdf')#, dpi=300)
plt.show()      
















