# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 02:53:38 2016

@author: xiaolin
"""

import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
import sys

plt.interactive(False)

def calLowerbound(X):
    sortedX = np.sort(X,axis =0)
    return sortedX[1,:]
    
    
def calUpperbound(X): 
    sortedX = np.sort(X, axis =0)
    return sortedX[X.shape[0]-1,:]   

def PlotStatesTracking(X_all, x_all_true, ind_meas,X_meas,t_all):
    #print X_all.shape
    #print x_all_true.shape
    #print ind_meas
    #print X_meas
    
    fig, axes = plt.subplots(nrows = 5, ncols=8,figsize=(16, 10) )
    for nrow in range(5):
        for ncol in range(8):
            #for s in range(30):
                #if (nrow*8 + ncol) == 0. and s==0.:
                    #axes[nrow, ncol].plot(t_all,X_all[s,nrow*8 + ncol,:],color = 'y', linewidth = 2,zorder=1, label="ensemble")
                #else:
                    #axes[nrow, ncol].plot(t_all,X_all[s,nrow*8 + ncol,:],color = 'y', linewidth = 2,zorder=1)                    
            lowerBound = calLowerbound(X_all[:,nrow*8 + ncol,:])
            upperBound = calUpperbound(X_all[:,nrow*8 + ncol,:])
            if (nrow*8 + ncol) == 0.:
                #axes[nrow, ncol].plot(t_all, lowerBound,color = 'k',linewidth = 1,zorder=2,label = '95% CI')
                #axes[nrow, ncol].plot(t_all, upperBound,color = 'k',linewidth = 1,zorder=2)
                axes[nrow, ncol].fill_between(t_all, lowerBound, upperBound, facecolor='blue',alpha=0.5, interpolate=True,label = '95%CI')
                axes[nrow, ncol].plot(t_all, x_all_true[:,nrow*8 + ncol],'r',linewidth = 1,label = 'truth')
                axes[nrow, ncol].text( 0.15,0.9,r'$x_{%d}$'%(nrow*8 + ncol+1), horizontalalignment='center', verticalalignment='center',transform = axes[nrow, ncol].transAxes, color='r',fontsize=15)
                axes[nrow, ncol].plot(t_all, np.mean(X_all[:,nrow*8 + ncol,:],axis = 0),color ='b',linewidth = 1,label = 'estimate')
                #axes[nrow,ncol].scatter(t_all[ind_meas],X_meas[:,nrow*8 + ncol],s = 15,color='b', marker='o',zorder=3,label = 'meas.')
                axes[nrow,ncol].legend(loc='lower left',numpoints=1,fancybox=False, shadow=False,ncol=5, bbox_to_anchor=(7.0, 1.05))
                #legend.draw_frame(False)
            #axes[nrow, ncol].set_xticklabels([0.0,0.5,1.0,1.5,2.0])
            else:
                #axes[nrow, ncol].plot(t_all, lowerBound,color = 'k',linewidth = 1,zorder=2,label = '95% CI')
                #axes[nrow, ncol].plot(t_all, upperBound,color = 'k',linewidth = 1,zorder=2)
                axes[nrow, ncol].fill_between(t_all, lowerBound, upperBound, facecolor='blue',alpha=0.5, interpolate=True)                
                axes[nrow, ncol].plot(t_all, x_all_true[:,nrow*8 + ncol],'r',linewidth = 1,label = 'truth')
                axes[nrow, ncol].text( 0.15,0.9,r'$x_{%d}$'%(nrow*8 + ncol+1), horizontalalignment='center', verticalalignment='center',transform = axes[nrow, ncol].transAxes, color='r',fontsize=15)
                axes[nrow, ncol].plot(t_all, np.mean(X_all[:,nrow*8 + ncol,:],axis = 0),color ='b',linewidth = 1,label = 'estimate')
                #if ncol%2 == 0:
                    #axes[nrow,ncol].scatter(t_all[ind_meas],X_meas[:,(nrow*8 + ncol)/2],s = 15,color='b', marker='o',zorder=3)
            axes[nrow, ncol].set_xticklabels([0.0,0.5,1.0,1.5,2.0])
            axes[nrow, ncol].set_ylim([-30,30])
            axes[nrow, ncol].set_xlim([0.0,2])
            axes[nrow, ncol].locator_params(axis='y',nbins=5)
    plt.savefig('figures/EnKF_linear_8.eps',format='eps')
    plt.savefig('figures/EnKF_linear_8.png',format='png')
    plt.show()