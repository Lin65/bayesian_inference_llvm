__author__ = 'terejanu'


import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from scipy.integrate import odeint
import llmf.LinearLatentModelFiltering as llmf
import sys

plt.interactive(False)
np.random.seed(seed=50)

# ------------------------------------------------------------------------------------------
# puff model
# ------------------------------------------------------------------------------------------
def puff_model_1D(aW, aX0):
    m = 1000
    p = 0.8
    q = 0.8
    t = 1
    w = aW
    x0 = aX0
    x = 0.  
    coeff =  m/np.sqrt(2*np.pi*(p**2)*((t*w)**(2*q)))
    return coeff*np.exp(-0.5*(x0 + t*w -x)**2/((p**2)*((t*w)**(2*q))))
# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 10000
no_comp = 3


nom_W = 10.0
std_W = 1.0

nom_X0 = 0.
std_X0 = 1000.


meas_var = 1

no_samples_hist = 10000
# ------------------------------------------------------------------------------------------
# generate measurements
# ------------------------------------------------------------------------------------------
no_observables = 1

true_W = 7.0
true_X0 = 1.0


true_var = np.array([true_W, true_X0])

X_meas =  puff_model_1D(true_W, true_X0) + np.random.normal(0, np.sqrt(meas_var))

# Plot truth
# Z = zs.reshape(X_mesh.shape)
# plt.figure()
# plt.contourf(X_mesh, Y_mesh, Z, 20, cmap=plt.get_cmap('Blues'))
# plt.show()

# ------------------------------
# initial condition - draw N samples
# ------------------------------
X_all = np.zeros((no_samples, 2))
X_all[:, 0] = np.random.normal(loc=nom_W, scale=std_W, size=no_samples)
X_all[:, 1] = np.random.normal(loc=nom_X0, scale=std_X0, size=no_samples)
X_all_old = X_all
#---------------------------------------------------------------------------
# EnKF 
#
# [y1,        [1  0  0  0  0 ...,       [ x1,
#  y2,   =     0  1  0  0  0 ...,  *      x2,                +  noise ~ N(0, I100)
#  y3,         0  0  1  0  0 ...,         x3,
#  y4,         0  0  0  1  0 ...,         x4,
#  y5         0  0  0  0  1 ...]          x5,
#  .                                       .
#  y100                                   x100
#                                         .]
#  X : (100+4)*no_samples,   H: 100*104
#                             
#
# meas_data: 1*100
#
#---------------------------------------------------------------------------

def EnKFupdate(X, H, R, meas_data):
    num_smps = X.shape[1]
    # Get the sampling covariance of states
    X_mu = np.mean(X, axis = 1)
    Ex = X - np.tile(X_mu, (num_smps, 1)).transpose()
    Pxx = 1./(num_smps -1) * np.dot(Ex, Ex.transpose())
 
    # Get the perturbed measurement covariance
    Ee = np.dot(np.linalg.cholesky(R).transpose() , np.random.randn(no_observables, num_smps))
    Re = 1./(num_smps - 1) * np.dot(Ee, Ee.transpose())

    
    # tile the perturbed measurement
    meas_pert = np.tile(meas_data, (num_smps, 1)).transpose() + Ee
    
    # Update the ensembles
    PH = np.dot(Pxx, H.transpose())
    
    # avoid singular matrix
    addsmall = 0#np.eye(np.shape(Re)[0])*1.0e-100
    INV = np.linalg.inv(np.dot(H, PH) + Re + addsmall)

    RES = meas_pert - np.dot(H, X) #+ 0.005

    X_update = X + np.dot(np.dot(PH, INV), RES)
    
    
    return X_update   


# ------------------------------------------------------------------------------------------
# get RMSE for a specific method
# ------------------------------------------------------------------------------------------


X_prior = X_all.copy()
X_prior_obs = np.zeros((no_samples,no_observables))

for i in range(no_samples):
    X_prior_obs[i,:] = puff_model_1D(X_prior[i,0], X_prior[i,1]) + np.random.normal(0, np.sqrt(meas_var))

X_prior_joint = np.hstack((X_prior_obs, X_prior))
H = np.concatenate((np.eye(no_observables),np.zeros((no_observables,2))),axis =1)
R = np.eye(no_observables)*meas_var
            
X_post = EnKFupdate(X_prior_joint.T, H, R, X_meas)

plt.figure()
plt.hist(X_post[1,:],bins=100)
plt.show()


