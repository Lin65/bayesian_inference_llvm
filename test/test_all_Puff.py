__author__ = 'terejanu'


import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from scipy.integrate import odeint
import llmf.LinearLatentModelFiltering as llmf
import sys

plt.interactive(False)
np.random.seed(seed=50)

# ------------------------------------------------------------------------------------------
# puff model
# ------------------------------------------------------------------------------------------
def puff_model(H,K,Q,u,x,y):
    #return np.exp(np.log(Q) - np.log(2.0*np.pi*K*x) - u*(y**2+H**2)/(4.0*K*x))
    return np.log(Q) - np.log(2.0*np.pi*K*x) - u*(y**2+H**2)/(4.0*K*x)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 30
no_comp = 4

no_sens_x = 10
no_sens_y = 10
no_observables = no_sens_x*no_sens_y

x_grid = np.linspace(0.01,5,no_sens_x)
y_grid = np.linspace(0.01,5,no_sens_y)
X_mesh, Y_mesh = np.meshgrid(x_grid, y_grid)

nom_H = 10.0
std_H = 1.0

nom_K = 1.0
std_K = 0.1

nom_Q = 1.0
std_Q = 0.1

nom_u = 1.0
std_u = 0.1

meas_var = 0.1**2

no_samples_hist = 10000

# ------------------------------------------------------------------------------------------
# generate measurements
# ------------------------------------------------------------------------------------------

true_H = 7.0
true_K = 1.0
true_Q = 1.0
true_u = 1.0

true_var = np.array([true_H, true_K, true_Q, true_u])

zs = np.array([puff_model(true_H,true_K,true_Q,true_u,x,y) for x,y in zip(np.ravel(X_mesh), np.ravel(Y_mesh))])

X_meas = zs.reshape((1,no_observables)) + np.random.multivariate_normal(np.zeros(no_observables),
                                                                        meas_var * np.eye(no_observables))

# Plot truth
# Z = zs.reshape(X_mesh.shape)
# plt.figure()
# plt.contourf(X_mesh, Y_mesh, Z, 20, cmap=plt.get_cmap('Blues'))
# plt.show()

# ------------------------------
# initial condition - draw N samples
# ------------------------------
X_all = np.zeros((no_samples, 4))
X_all[:, 0] = np.random.normal(loc=nom_H, scale=std_H, size=no_samples)
X_all[:, 1] = np.random.normal(loc=nom_K, scale=std_K, size=no_samples)
X_all[:, 2] = np.random.normal(loc=nom_Q, scale=std_Q, size=no_samples)
X_all[:, 3] = np.random.normal(loc=nom_u, scale=std_u, size=no_samples)
X_all_old = X_all

# ------------------------------------------------------------------------------------------
# get RMSE for a specific method
# ------------------------------------------------------------------------------------------
def get_RMSE(run_model = 'PCCA', run_ensemble=True, run_lvm = True):

    X_prior = X_all.copy()
    X_prior_obs = np.zeros((no_samples,no_observables))

    for i in range(no_samples):
        zs = np.array([puff_model(X_prior[i,0], X_prior[i,1], X_prior[i,2], X_prior[i,3], x, y) for x, y in zip(np.ravel(X_mesh), np.ravel(Y_mesh))])
        X_prior_obs[i,:] = zs.reshape((1, no_observables)) + np.random.multivariate_normal(np.zeros(no_observables), meas_var * np.eye(no_observables))

    X_prior_joint = np.hstack((X_prior, X_prior_obs))

    # scale the samples
    trans_mean = np.mean(X_prior_joint, axis=0)
    trans_std = np.std(X_prior_joint, axis=0)

    tile_trans_mean = np.tile(trans_mean, (no_samples, 1))
    tile_trans_std = np.tile(trans_std, (no_samples, 1))
    X_prior_joint_trans = (X_prior_joint - tile_trans_mean) / tile_trans_std

    # scale the measurement
    X_meas_trans = (X_meas - trans_mean[4:4 + no_observables]) / trans_std[4:4 + no_observables]

    if run_ensemble:
        llmf_en_ppca = llmf.EnsembleLinearLatentModel(run_model)
        llmf_en_ppca.fit(X_prior_joint_trans, nx=4, nz=no_comp)
        if run_lvm:
            llmf_en_ppca.update_lvm_noise(X_meas_trans)
        else:
            llmf_en_ppca.update_models(X_meas_trans)
        X_post_trans = llmf_en_ppca.generate_samples(no_samples_hist)
        est_mu = llmf_en_ppca.get_mean_qoi()
    else:
        direct_lvm = llmf.DirectLinearLatentModel(run_model)
        direct_lvm.fit(X_prior_joint, nx=4, nz=no_comp)
        if run_lvm:
            direct_lvm.update_lvm_noise(X_meas_trans)
        else:
            direct_lvm.update_model(X_meas_trans)
        X_post_trans = direct_lvm.generate_samples(no_samples_hist)

    X_post = X_post_trans * np.tile(trans_std[0:4], (no_samples_hist, 1)) + np.tile(trans_mean[0:4], (no_samples_hist, 1))

    # plt.figure()
    # plt.subplot(221)
    # plt.hist(X_post[:,0],bins=100)
    # plt.subplot(222)
    # plt.hist(X_post[:, 1], bins=100)
    # plt.subplot(223)
    # plt.hist(X_post[:, 2], bins=100)
    # plt.subplot(224)
    # plt.hist(X_post[:, 3], bins=100)
    # plt.show()

    est_mu = np.mean(X_post, axis=0)
    #print est_mu
    return np.sqrt(np.mean((est_mu - true_var) ** 2))

# ------------------------------------------------------------------------------------------
# average performance over a number of runs
# ------------------------------------------------------------------------------------------
def avg_performance(run_model, no_runs):

    RMSE = np.zeros((no_runs,))
    for i in range(no_runs):
        RMSE[i] = get_RMSE(run_model)
        print 'RMSE ( ', run_model, ', ', i, ' ) = ', RMSE[i]

    return RMSE.mean(), RMSE.std()

# ------------------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------------------
if __name__ == '__main__':

    no_runs = 100

    #X_all = X_all_old
    #PCCA_mean, PCCA_std = avg_performance('PCCA', no_runs)
    #print 'PCCA_mean = ', PCCA_mean
    #print 'PCCA_std  = ', PCCA_std

    X_all = X_all_old
    PPCA_mean, PPCA_std = avg_performance('PPCA', no_runs)
    print 'PPCA_mean = ', PPCA_mean
    print 'PPCA_std  = ', PPCA_std

    #X_all = X_all_old
    #FA_mean, FA_std = avg_performance('FA', no_runs)
    #print 'FA_mean   = ', FA_mean
    #print 'FA_std    = ', FA_std