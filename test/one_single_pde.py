import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
import llmf.LinearLatentModelFiltering as llmf
import sys

plt.interactive(False)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 40000
no_comp = 3
dim  = 4
dim_x =1
# ----------------------------
# generate samples
# ----------------------------
np.random.seed(seed=50)

mu = np.random.normal(0,1,dim)
A = np.random.normal(0,1,(dim,dim))
cov = 0.5*(A + A.T) + dim*np.eye(dim)
smps = np.random.multivariate_normal(mu, cov, no_samples) * np.random.multivariate_normal(mu+5, cov*10, no_samples)

# ------------------------------
# pde
# ------------------------------
run_model = 'PPCA'

trans_mean = np.mean(smps, axis=0)
trans_std = np.std(smps, axis=0)
trans_w = np.diag(trans_std)

tile_trans_mean = np.tile(trans_mean, (no_samples, 1))
tile_trans_std = np.tile(trans_std, (no_samples, 1))
smps_trans = (smps - tile_trans_mean) / tile_trans_std


direct_lvm = llmf.DirectLinearLatentModel(run_model)
direct_lvm.fit(smps_trans, nx=dim_x, nz=no_comp)

mu_post = trans_std*direct_lvm.mu_y_post.T + trans_mean
Sigma_post = np.dot(np.dot(trans_w,direct_lvm.Sigma_y_post), trans_w.T)

print 'mu_post: \n', mu_post 
print 'Sigma_post: \n',Sigma_post
print 'W_y: \n', direct_lvm.W_y
print 'Psi_y: \n', direct_lvm.Psi_y
print 'mu_y: \n', direct_lvm.mu_y

plt.plot(smps[:,0], smps[:,1],'.')
plt.show()

#direct_lvm.update_model(X_meas_trans)
#X_post_trans = direct_lvm.generate_samples()


