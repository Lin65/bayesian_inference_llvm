# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 02:53:38 2016

@author: xiaolin
"""

from utils import *
import matplotlib.pyplot as plt
import numpy as np
import sys

plt.interactive(False)

def calLowerbound(X):
    sortedX = np.sort(X,axis =0)
    return sortedX[1,:]
    
    
def calUpperbound(X): 
    sortedX = np.sort(X, axis =0)
    return sortedX[X.shape[0]-2,:]   

def PlotStatesTracking(X_all, x_all_true, ind_meas,X_meas,t_all):
    #print X_all.shape
    #print x_all_true.shape
    #print ind_meas
    #print X_meas
    plt.figure()

    for s in range(X_all.shape[0]):
            #print X_all.shape[0]
        plt.plot(t_all,X_all[s,:],color = 'y', linewidth = 2,zorder=1)
    
    lowerBound = calLowerbound(X_all)
    upperBound = calUpperbound(X_all)

    plt.plot(t_all, lowerBound,color = 'k',linewidth = 1,zorder=2,label = '95% CI')
    plt.plot(t_all, upperBound,color = 'k',linewidth = 1,zorder=2)
    plt.plot(t_all, x_all_true,'r',linewidth = 1,label = 'truth')
            #axes[nrow].text( 0.15,0.9,r'$x_{%d}$'%nrow, horizontalalignment='center', verticalalignment='center',transform = axes[nrow].transAxes, color='r',fontsize=15)
            #plt.pyplot(t_all, np.mean(X_all,axis = 0),color ='b',linewidth = 1,label = 'estimate')

            #axes[nrow, ncol].set_xticklabels([0.0,0.5,1.0,1.5,2.0])
            #axes[nrow, ncol].set_xlim([0.0,2.0])
            #axes[nrow].locator_params(axis='y',nbins=5)
            #plt.savefig('figures/PPCA_nonlinear_square_12.eps',format='eps', dpi=300)
    plt.savefig('figures/mixture_1d_traj.pdf',format='pdf', dpi=200)
    plt.show()