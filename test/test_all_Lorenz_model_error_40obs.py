__author__ = 'terejanu'


import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
import llmf.LinearLatentModelFiltering as llmf
import sys
from StatesTrackingPlot import PlotStatesTracking 

plt.interactive(False)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 30
no_comp = 5
meas_freq = 200

no_observables = 40

meas_var = 1.0
prior_std = 1.0

# ------------------------------
# time span
# ------------------------------
t_init = 0
t_final = 2.0
t_delta = 0.001
t_all = np.arange(t_init, t_final, t_delta)
ind_meas = range(meas_freq, t_all.shape[0], meas_freq)

np.random.seed(seed=50)
# ------------------------------
# true initial condition
# ------------------------------
x_init_true = np.random.normal(loc=0.0, scale=prior_std, size=dim_lorenz)

# ------------------------------
# generate measurements
# ------------------------------
x_all_true = odeint(lorenz96, x_init_true, t_all)
obs_list = range(0,40,1)

# X_meas = np.zeros((len(ind_meas), dim_lorenz/2))
X_meas = np.zeros((len(ind_meas), no_observables))
for i in range(len(ind_meas)):
    # tmp_state, tmp_meas = foward_trans_state_obs( x_all_true[ind_meas[i]] )
    # X_meas[i, :] = tmp_meas + np.random.multivariate_normal(np.zeros(dim_lorenz/2), meas_var*np.eye(dim_lorenz/2))
    tmp_meas = x_all_true[ind_meas[i], obs_list]
    X_meas[i, :] = tmp_meas + np.random.multivariate_normal(np.zeros(no_observables),
                                                            meas_var * np.eye(no_observables))
# ------------------------------
# initial condition - draw N samples
# ------------------------------
X_all = np.zeros((no_samples, dim_lorenz, len(t_all)))

for i in range(no_samples):
    X_all[i, :, 0] = np.random.normal(loc=0.0, scale=prior_std, size=dim_lorenz)

X_all_old = X_all

# ------------------------------------------------------------------------------------------
# get RMSE for a specific method
# ------------------------------------------------------------------------------------------
def get_RMSE(run_model = 'PPCA', run_ensemble=True, run_lvm = True, const=8.):

    # ------------------------------
    # TIME LOOP
    # ------------------------------
    for k in range(len(ind_meas) + 1):

        # propagate samples forward in time
        if k == 0:
            ind_init = 0
            ind_final = ind_meas[k] + 1
        elif k == len(ind_meas):
            ind_init = ind_meas[k - 1]
            ind_final = len(t_all)
        else:
            ind_init = ind_meas[k - 1]
            ind_final = ind_meas[k] + 1

        #print '- propagate from t = ', t_all[ind_init], ' [s] to t = ', t_all[ind_final - 1], ' [s]'
        sys.stdout.flush()

        for i in range(no_samples):
            x_result_tmp = odeint(lorenz96_model_error, X_all[i, :, ind_init], t_all[ind_init:ind_final], args = (const,))
            X_all[i, :, ind_init:ind_final] = x_result_tmp.T

        # update samples - inversion using EnPPCA
        if k < len(ind_meas):

            ind_update = ind_meas[k]

            #print '- update at t = ', t_all[ind_update]
            sys.stdout.flush()

            # obtain samples and transform them
            X_prior = X_all[:, :, ind_update]
            # X_prior_obs = X_prior[:,0::2]
            X_prior_obs = X_prior[:, obs_list]

            # add measurement noise to the observable
            for j in range(no_samples):
                # X_prior_obs[j,:] = X_prior_obs[j,:] + np.random.multivariate_normal(np.zeros(dim_lorenz/2), meas_var*np.eye(dim_lorenz/2))
                X_prior_obs[j, :] = X_prior_obs[j, :] + np.random.multivariate_normal(np.zeros(no_observables),
                                                                                      meas_var * np.eye(no_observables))

            X_prior_joint = np.hstack((X_prior, X_prior_obs))

            # standarization
            trans_mean = np.mean(X_prior_joint, axis=0)
            trans_std = np.std(X_prior_joint, axis=0)

            tile_trans_mean = np.tile(trans_mean, (no_samples, 1))
            tile_trans_std = np.tile(trans_std, (no_samples, 1))
            X_prior_joint_trans = (X_prior_joint - tile_trans_mean) / tile_trans_std

            X_meas_trans = (X_meas[k, :] - trans_mean[40:40 + no_observables]) / trans_std[40:40 + no_observables]

            #-------------------------------------------------------------------
            if run_ensemble:
                llmf_en_ppca = llmf.EnsembleLinearLatentModel(run_model)
                llmf_en_ppca.fit(X_prior_joint_trans, nx=dim_lorenz, nz=no_comp)
                if run_lvm:
                    llmf_en_ppca.update_lvm_noise(X_meas_trans)
                else:
                    llmf_en_ppca.update_models(X_meas_trans)
                X_post_trans = llmf_en_ppca.generate_samples()
                # mu_est[:,k] = llmf_en_ppca.get_mean_qoi() * np.reshape(trans_std[0:40],(40,1)) + np.reshape(trans_mean[0:40],(40,1))
            else:
                direct_lvm = llmf.DirectLinearLatentModel(run_model)
                direct_lvm.fit(X_prior_joint, nx=dim_lorenz, nz=no_comp)
                if run_lvm:
                    direct_lvm.update_lvm_noise(X_meas_trans)
                else:
                    direct_lvm.update_model(X_meas_trans)
                X_post_trans = direct_lvm.generate_samples()
			#-------------------------------------------------------------------

            X_post = X_post_trans * tile_trans_std[:, 0:40] + tile_trans_mean[:, 0:40]
            X_all[:, :, ind_update] = X_post
    #PlotStatesTracking(X_all, x_all_true, ind_meas,X_meas,t_all)

    # ------------------------------
    # Print
    # ------------------------------
    RMSE = np.zeros((dim_lorenz,))
    for k in range(1, dim_lorenz + 1):
        est_mu = np.mean(X_all[:, k - 1, :], axis=0)
        est_std = np.std(X_all[:, k - 1, :], axis=0)
        RMSE[k - 1] = np.mean((est_mu - x_all_true[:, k - 1]) ** 2)
    #print RMSE
    return np.sqrt(RMSE.mean())

# ------------------------------------------------------------------------------------------
# average performance over a number of runs
# ------------------------------------------------------------------------------------------
def avg_performance(run_model, no_runs,const):

    RMSE = np.zeros((no_runs,))
    for i in range(no_runs):
        RMSE[i] = get_RMSE(run_model,True, True, const)
        print 'RMSE ( ', run_model, ', ', i, ' ) = ', RMSE[i]

    return RMSE.mean(), RMSE.std()

# ------------------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------------------
if __name__ == '__main__':

    no_runs = 10

    X_all = X_all_old
    for const in [12.]:
        PPCA_mean, PPCA_std = avg_performance('PPCA', no_runs,const)
        print 'const ',const, ' PPCA_mean= ', PPCA_mean,'  PPCA_std= ', PPCA_std

    #X_all = X_all_old
    #PCCA_mean, PCCA_std = avg_performance('PCCA', no_runs)
    #print 'PCCA_mean = ', PCCA_mean
    #print 'PCCA_std  = ', PCCA_std

    # X_all = X_all_old
    # FA_mean, FA_std = avg_performance('FA', no_runs)
    # print 'FA_mean   = ', FA_mean
    # print 'FA_std    = ', FA_std

