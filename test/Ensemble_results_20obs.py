# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 02:53:38 2016

@author: xiaolin
"""

import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
import llmf.LinearLatentModelFiltering as llmf
import sys
from StatesTrackingPlot import PlotStatesTracking 

plt.interactive(False)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 30
no_comp = 5
meas_freq = 1#200

no_observables = 20

meas_var = 1.0 #1.0
prior_std = 1.0

# ------------------------------
# time span
# ------------------------------
t_init = 0
t_final = 10#2.0
t_delta = 0.05 #0.001
t_all = np.arange(t_init, t_final, t_delta)
ind_meas = range(meas_freq, t_all.shape[0], meas_freq)

np.random.seed(seed=50)
# ------------------------------
# true initial condition
# ------------------------------
x_init_true = np.random.normal(loc=0.0, scale=prior_std, size=dim_lorenz)

# ------------------------------
# generate measurements
# ------------------------------
x_all_true = odeint(lorenz96, x_init_true, t_all)
obs_list = range(0,40,2)

# X_meas = np.zeros((len(ind_meas), dim_lorenz/2))
X_meas = np.zeros((len(ind_meas), no_observables))
for i in range(len(ind_meas)):
    # tmp_state, tmp_meas = foward_trans_state_obs( x_all_true[ind_meas[i]] )
    # X_meas[i, :] = tmp_meas + np.random.multivariate_normal(np.zeros(dim_lorenz/2), meas_var*np.eye(dim_lorenz/2))
    tmp_meas = x_all_true[ind_meas[i], obs_list]
    X_meas[i, :] = tmp_meas + np.random.multivariate_normal(np.zeros(no_observables),
                                                            meas_var * np.eye(no_observables))
# ------------------------------
# initial condition - draw N samples
# ------------------------------
X_all = np.zeros((no_samples, dim_lorenz, len(t_all)))

for i in range(no_samples):
    X_all[i, :, 0] = np.random.normal(loc=0.0, scale=prior_std, size=dim_lorenz)

X_all_old = X_all

#---------------------------------------------------------------------------
# EnKF 
#
# [y1,        [1  0  0  0  0 ...,       [ x1,
#  y2,   =     0  0  1  0  0 ...,  *      x2,                +  noise ~ N(0, I20)
#  y3,         0  0  0  0  1 ...,         x3,
#  y4,         ...                        x4,
#  y5,         0  0  0  0  0 ...]         x5,
#  .                                       .
#  .                                       .
#  y20]                                   x40]
#
#                                       
#  X : 40*no_samples,   H: 20*40
#                             
#
# meas_data: 1*20
#
#---------------------------------------------------------------------------

def EnKFupdate(X, H, R, meas_data):
    num_smps = X.shape[1]
    # Get the sampling covariance of states
    X_mu = np.mean(X, axis = 1)
    Ex = X - np.tile(X_mu, (num_smps, 1)).transpose()
    Pxx = 1./(num_smps -1) * np.dot(Ex, Ex.transpose())
    
    
    
    #noise inflation
    res = meas_data.transpose() - np.dot(H, X_mu.transpose())
    lamda = 1./R.shape[0]* np.matrix.trace(np.dot(np.dot(res, res.transpose()) - np.dot(np.dot(H, Pxx), H.transpose()), R))
    if lamda < 1.:
        lamda = 1
    #print 'lamda', lamda 
    #R_origin = np.copy(R)
    R = lamda*R
        
 
    # Get the perturbed measurement covariance
    Ee = np.dot(np.linalg.cholesky(R).transpose() , np.random.randn(no_observables, num_smps))
    Re = 1./(num_smps - 1) * np.dot(Ee, Ee.transpose())
  
    
    # tile the perturbed measurement
    meas_pert = np.tile(meas_data, (num_smps, 1)).transpose() + Ee
    
    RES = meas_pert - np.dot(H, X) #+ 0.005
    
    # forecast error covariance matrix inflation
    #fore_res = meas_data.transpose() - np.dot(H, X_mu.transpose())
    #RD = np.dot(np.linalg.inv(np.sqrt(R_origin)),fore_res)
    #RH = np.dot(np.linalg.inv(np.sqrt(R_origin)),H)
    #TRACE =  np.trace(np.dot(np.dot(RH,Pxx), RH.transpose())) 
    
    #fore_lamda = (np.dot(RD.transpose(),RD) - R.shape[0])/TRACE
    #print fore_lamda
    #Pxx = Pxx*fore_lamda
    
    # Update the ensembles
    PH = np.dot(Pxx, H.transpose())


    # avoid singular matrix
    addsmall = 0#np.eye(np.shape(Re)[0])*1.0e-100
    INV = np.linalg.inv(np.dot(H, PH) + Re + addsmall)

    X_update = X + np.dot(np.dot(PH, INV), RES)
    
    
    return X_update   

# ------------------------------------------------------------------------------------------
# get RMSE for a specific method
# ------------------------------------------------------------------------------------------
def get_RMSE():
    # ------------------------------
    # TIME LOOP
    # ------------------------------
    for k in range(len(ind_meas) + 1):
        
        # propagate samples forward in time
        if k == 0:
            ind_init = 0
            ind_final = ind_meas[k] + 1
        elif k == len(ind_meas):
            ind_init = ind_meas[k - 1]
            ind_final = len(t_all)
        else:
            ind_init = ind_meas[k - 1]
            ind_final = ind_meas[k] + 1

        #print '- propagate from t = ', t_all[ind_init], ' [s] to t = ', t_all[ind_final - 1], ' [s]'
        sys.stdout.flush()

        for i in range(no_samples):
            x_result_tmp = odeint(lorenz96, X_all[i, :, ind_init], t_all[ind_init:ind_final])
            X_all[i, :, ind_init:ind_final] = x_result_tmp.T

        # update samples - inversion using EnKF
        if k < len(ind_meas):

            ind_update = ind_meas[k]

            #print '- update at t = ', t_all[ind_update]
            sys.stdout.flush()

            # obtain samples and transform them
            X_prior = X_all[:, :, ind_update]
            
            #H = np.concatenate((np.eye(no_observables),np.zeros((no_observables,dim_lorenz - no_observables))),axis =1)
            
            H = np.zeros((no_observables, dim_lorenz)) 
            
            for i in range(no_observables):
                H[i,2*i] = 1.
                            
            R = np.eye(no_observables)*meas_var
            
            X_post = EnKFupdate(X_prior.T, H, R, X_meas[k, :])
            X_all[:, :, ind_update] = X_post.T
    #PlotStatesTracking(X_all, x_all_true, ind_meas,X_meas,t_all)
    # ------------------------------
    # Print
    # ------------------------------
    RMSE = np.zeros((dim_lorenz,))
    for k in range(1, dim_lorenz + 1):
        est_mu = np.mean(X_all[:, k - 1, :], axis=0)
        est_std = np.std(X_all[:, k - 1, :], axis=0)
        RMSE[k - 1] = np.mean((est_mu - x_all_true[:, k - 1]) ** 2)
        
        #print RMSE

    return np.sqrt(RMSE.mean())

# ------------------------------------------------------------------------------------------
# average performance over a number of runs
# ------------------------------------------------------------------------------------------
def avg_performance(no_runs):

    RMSE = np.zeros((no_runs,))
    for i in range(no_runs):
        RMSE[i] = get_RMSE()
        print 'RMSE ', i, ' = ', RMSE[i]

    return RMSE.mean(), RMSE.std()

# ------------------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------------------
if __name__ == '__main__':

    no_runs = 100

    X_all = X_all_old
    Ensemble_mean, Ensemble_std = avg_performance(no_runs)
    print 'Ensemble_mean = ', Ensemble_mean
    print 'Ensemble_std  = ', Ensemble_std

    # X_all = X_all_old
    # PCCA_mean, PCCA_std = avg_performance('PCCA', no_runs)
    # print 'PCCA_mean = ', PCCA_mean
    # print 'PCCA_std  = ', PCCA_std

    # X_all = X_all_old
    # FA_mean, FA_std = avg_performance('FA', no_runs)
    # print 'FA_mean   = ', FA_mean
    # print 'FA_std    = ', FA_std



















