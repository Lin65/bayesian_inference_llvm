import matplotlib as mpl
mpl.use('Qt4Agg')

from utils import *
import matplotlib.pyplot as plt
import numpy as np
import llmf.LinearLatentModelFiltering as llmf
import sys

plt.interactive(False)

# ------------------------------------------------------------------------------------------
# global variables
# ------------------------------------------------------------------------------------------
no_samples = 10000
no_comp = 1
dim  = 3
dim_x = 2
# ----------------------------
# generate samples
# ----------------------------
np.random.seed(seed=40)

mu = np.random.normal(0,1,dim)
A = np.random.normal(0,1,(dim,dim))
cov = 0.5*(A + A.T) + dim*np.eye(dim)

smps = np.random.multivariate_normal(mu, cov, no_samples) 

# ----------------------------
# generate meas
# ----------------------------

meas = 2

truth_x_mu = mu[0:dim_x] + cov[0:dim_x, dim_x:dim].dot(np.linalg.inv(cov[dim_x:dim, dim_x:dim])).dot(meas - mu[dim_x:dim]).transpose()
truth_x_sigma = cov[0:dim_x, 0:dim_x] - cov[0:dim_x, dim_x:dim].dot(np.linalg.inv(cov[dim_x:dim, dim_x:dim])).dot(cov[0:dim_x, dim_x:dim].transpose())

print 'truth_x_mu: ',truth_x_mu
print 'truth_x_sigma: ',truth_x_sigma

# ------------------------------
# pde
# ------------------------------
run_model = 'PPCA'

trans_mean = np.mean(smps, axis=0)
trans_std = np.std(smps, axis=0)
trans_w = np.diag(trans_std)

tile_trans_mean = np.tile(trans_mean, (no_samples, 1))
tile_trans_std = np.tile(trans_std, (no_samples, 1))
smps_trans = (smps - tile_trans_mean) / tile_trans_std

meas_trans = (meas - trans_mean[dim_x:dim]) / trans_std[dim_x:dim]


direct_lvm = llmf.DirectLinearLatentModel(run_model)
direct_lvm.fit(smps_trans, nx=dim_x, nz=no_comp)

#direct_lvm.update_lvm_noise(meas_trans)
direct_lvm.update_model(meas_trans)

X_post_trans = direct_lvm.generate_samples()
			
X_smps_post = X_post_trans * tile_trans_std[:,0:dim_x] + tile_trans_mean[:,0:dim_x]

mu_post = trans_std*direct_lvm.mu_y_post.T + trans_mean
Sigma_post = np.dot(np.dot(trans_w,direct_lvm.Sigma_y_post), trans_w.T)

print 'x_post_mu: ', mu_post[0,0:dim_x]
print 'x_post_sigma:, ',Sigma_post[0:dim_x, 0:dim_x]


#plt.hist(X_smps_post[:,0])
#plt.show()
'''

mu_post = trans_std*direct_lvm.mu_y_post.T + trans_mean
Sigma_post = np.dot(np.dot(trans_w,direct_lvm.Sigma_y_post), trans_w.T)

print 'mu_post: \n', mu_post 
print 'Sigma_post: \n',Sigma_post
print 'W_y: \n', direct_lvm.W_y
print 'Psi_y: \n', direct_lvm.Psi_y
print 'mu_y: \n', direct_lvm.mu_y

plt.plot(smps[:,0], smps[:,1],'.')
plt.show()
'''
#direct_lvm.update_model(X_meas_trans)
#X_post_trans = direct_lvm.generate_samples()


